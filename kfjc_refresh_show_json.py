import requests
from bs4 import BeautifulSoup
import json
import time

'''

This gets the newest version of the archive page and makes a list of dicts
containing show info. you will have to assemble the URLs yourself if you 
want to download the files but that's easy enough.

'''

archive_url = 'https://kfjc.org/listen/archives'
krq = requests.get(archive_url)
soup = BeautifulSoup(krq.text,"html.parser")

shows = list()
for pee in soup.select(".col-9 > p"):
    files = list()
    show = {"dj":pee('b')[0].text}
    for anchor in pee('a'):
        txt = anchor['href']
        if txt[:19] == '/listen/playlist?i=':
            playlist_id = int(txt[19:])
            show["playlist_id"] = playlist_id
        if txt[:34] == 'https://archive.kfjc.org/archives/':
            files.append(txt[34:])
    show["files"] = files

    tz = "-0800"
    stamp = files[0][:10] + tz
    ustamp = time.mktime(time.strptime(stamp, "%y%m%d%H%M%z"))
    show["timestamp"] = ustamp
    shows.append(show)
    
json_file = "kfjc_shows.json"
with open(json_file, 'w') as outfile:
    json.dump(shows, outfile)
