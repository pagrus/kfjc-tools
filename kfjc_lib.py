import requests
import subprocess
from bs4 import BeautifulSoup
import json
# import pandas as pd
import time
import sys
from pathlib import Path

import os
from datetime import datetime

def get_config(home_dir):
    config_file = str(home_dir) + "/.config/mascon/kfjc.mcx"
    configs = dict()
    try:
        with open(config_file, "r") as cf:
            for c in cf:
                cs = c.split()
                configs[cs[0]] = cs[1]
    except IOError:
        configs["file_exists"] = False
        print("no configs found")
    return configs

def get_show_list():
    
    archive_url = 'https://kfjc.org/listen/archives'
    krq = requests.get(archive_url)
    soup = BeautifulSoup(krq.text,"html.parser")
    anchors = soup.find_all('a')

    mp3s = []
    olddj = ''
    txt = ''
    shows = []
    this_show = ["show list"]
    playlists = []
    for anchor in anchors:
        txt = anchor['href']
        if txt[-4:] == '.mp3':
            mp3s.append(txt)
        if txt[:19] == '/listen/playlist?i=':
            playlists.append(int(txt[19:]))
    for mp3 in mp3s:
        stamp = mp3[26:36]
        yr = stamp[:2]
        mo = stamp[2:4]
        dt = stamp[4:6]
        hh = stamp[6:8]
        mm = stamp[8:10]
        dj = mp3[38:-4]
        # print(dj, yr, mo, dt, hh, mm)
        if dj != olddj:
            shows.append(this_show)
            this_show = []

            desc = "{}_{}_{}_{}_{}_{}".format(yr, mo, dt, hh, mm, dj)
            this_show.append(desc)
            this_show.append(mp3)
            olddj = dj
        else:
            this_show.append(mp3)
    rettup = (shows, playlists)
    return rettup

def print_show_list(shows, start, page):
    # print("### kfjc show getter v1.1 ###")
    end = start + page
    for idx, show in enumerate(shows[0]):
        if idx >= start and idx < end:
            if idx == 0:
                print("### show list ###")
            else:
                print(idx, show[0], shows[1][idx - 1])

def select_show_id():
    show_id_txt = input('select show number to download, 0 to cancel: ')
    show_id = int(show_id_txt)
    return show_id

def print_show_detail(shows, show_id):
    title = shows[0][show_id][0]
    file_q = len(shows[0][show_id]) - 1
    print("show", title, "selected, id", show_id, file_q, "files")
    
def fetch_show(shows, show_id, configs):
    for sidx, show_item in enumerate(shows[0][show_id]):

        if sidx == 0:
            kfjc_dir = configs['remote_dir'] + show_item
            login = configs['remote_user'] + "@" + configs['remote_machine_address']
            if configs['push'] == "on":
                print('creating directory {}...'.format(show_item))
                subprocess.call(["ssh", login, "mkdir", kfjc_dir])
        else:
            print("getting item:", show_item)
            mp3_resp = requests.get(show_item)
            filename = show_item[26:]
            path = configs['local_dir'] + "/" + filename
            if mp3_resp.status_code == 200:
                with open(path, 'wb') as fh:
                    fh.write(mp3_resp.content)
                if configs['push'] == 'on':
                    remote = login + ":" + kfjc_dir + "/" + filename
                    print("copying", filename, "to", configs['remote_machine_name']) 
                    subprocess.call(["scp", path, remote])
                if configs['keep_local'] == 'off':
                    subprocess.call(["rm", path])
    print("done!")

def find_old_files(configs):
    old_files = list()
    archive_dirs = set()
    rightnow = int(time.time())
    # cutoff specified in days, converted to seconds
    age_thresh = (60 * 60 * 24) * int(configs['age_cutoff'])
    # get dir from config
    file_dir = configs['local_dir']
    file_list = os.listdir(file_dir)

    for item in file_list:
        if item[-4:] == ".mp3":
            file_stamp = item.split("_")[0][:-1]
            stamp_object = datetime.strptime(file_stamp, '%y%m%d%H%M')
            archive_dir = stamp_object.strftime("%Y_%m")
            archive_path = "{}/{}".format(file_dir, archive_dir)
            file_delta = rightnow - stamp_object.timestamp()
            # print(stamp_object.timestamp())
            # print(file_delta)
            if file_delta > age_thresh:
                # if archive_dir not in file_list:
                    # os.mkdir(archive_path)
                days_old = round((file_delta / (60 * 60 * 24)), 2)
                old_path = "{}/{}".format(file_dir, item)
                new_path = "{}/{}/{}".format(file_dir, archive_dir, item)
                file_tup = (item, file_delta, days_old, old_path, new_path, archive_path)
                old_files.append(file_tup)
                archive_tup = (archive_dir, archive_path)
                archive_dirs.add(archive_tup)
    rettup = (sorted(old_files), archive_dirs)
    return (rettup)

def archive(file_info):
    file_list = file_info[0]
    dir_list = file_info[1]
    for archive_dir in dir_list:
        if not os.path.isdir(archive_dir[1]):
            print("creating directory {}".format(archive_dir[0]))
            os.makedirs(archive_dir[1])
    for old_file in file_list:
        print("moving file {} ({} days old)...".format(old_file[0], old_file[2]))
        os.rename(old_file[3], old_file[4])
