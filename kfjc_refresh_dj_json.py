import requests
from bs4 import BeautifulSoup
import json

dj_url = 'https://kfjc.org/listen/dj-list'
djrq = requests.get(dj_url)
soup = BeautifulSoup(djrq.text,"html.parser")

djs = dict()
for dj in soup.select("p > a"):
    link = dj['href']
    if link[:21] == "/listen/dj-info?djid=":
        dj_id = int(link[21:])
        dj_txt = dj.text
        djs[dj_id] = dj_txt
        
json_file = "kfjc_djs.json"
with open(json_file, 'w') as outfile:
    json.dump(djs, outfile)