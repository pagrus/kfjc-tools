import requests
import subprocess
from bs4 import BeautifulSoup
import json
# import pandas as pd
import time
import sys
from pathlib import Path

import os
from datetime import datetime

import kfjc_lib as kf

# get path to home dir, look for config
home_dir = Path.home()

# look for config file here /home/pagrus/.config/mascon
# get config info, looks like this:

# dl_dir /home/pagrus/Music/kfjc
# keep_local on
# push on
# destination salote 

# get list of available shows
# display list ask for input
# download indicated show

# get playlist from website using plid
# make indexed list, ask for selection
# log a like using item data

# download playlist, move to music dir
# menu options:
# number. get show
# r. refresh show list (gets new list from website)
# n. show next 10 listings
# p. show prev 10 listings
# q. quit

# figure out what host you're using
# act appropriately, eg
# if local, download directly to correct dir
# if remote, get remote paths and ask whether to keep local copies?
# wait, where am I? do I do anything differently if I'm at home
# or work?
    
configs = kf.get_config(home_dir)

old_files = kf.find_old_files(configs)
old_count = len(old_files[0])
if (old_count > 0):
    print("{} files found older than {} days:".format(old_count, configs['age_cutoff']))
    for old_file in old_files[0]:
        print(old_file[0], old_file[2])
    archive_choice = input('archive? y/n: '.format(old_count))
    if (archive_choice == "y"):
        print("archiving {} files...".format(old_count))
        kf.archive(old_files)
    else:
        print("not archiving {} files".format(old_count))

    
shows = kf.get_show_list()

if len(sys.argv) > 1:
    count = int(sys.argv[1])
else:
    count = 25

kf.print_show_list(shows, 0, count)

show_id = kf.select_show_id()
kf.print_show_detail(shows, show_id)

if (show_id > 0):
    kf.fetch_show(shows, show_id, configs)
